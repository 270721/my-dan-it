'use strict';

function createNewUser(firstName, lastName, birthday) {
	const newUser = {

		name: firstName,
		surname: lastName,
		date: birthday,
	
		getLogin() {
			return this.name[0].toLowerCase() + this.surname.toLowerCase();
		},
	
		getAge() {
			const today = new Date();
			const userBirthday = Date.parse(`${this.date.slice(6)}-${this.date.slice(3, 5)}-${this.date.slice(0, 2)}`);
			const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
			if (age < today) {
				return `You are ${age - 1} years old`;
			} else {
				return `You are ${age} years old`;
			}
		},
	
		getPassword() {
			return `${this.name[0].toUpperCase()}${this.surname.toLocaleLowerCase()}${this.date.slice(-4)}`
		},
	
	}

	return newUser;
}

const firstName = prompt("Your name");
const lastName = prompt("Your last name");
const birthday = prompt("Your birthday dd.mm.yyyy");

const result = createNewUser(firstName, lastName, birthday);

console.dir(result.getLogin());
console.dir(result.getAge());
console.dir(result.getPassword());
