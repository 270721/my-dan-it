'use strict';

const arr = ["1", "2", "3", "sea", "user", 23];

function makeList(arrayList, parent = document.body){
    
    const list = document.createElement("ul")

    const listItems = arrayList.map((el) => {
        return `<li>${el}</li>`
    });

    list.innerHTML = listItems.join('')
    parent.append(list)
}
makeList(arr);