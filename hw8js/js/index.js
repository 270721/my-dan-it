'use strict';

const input = document.querySelector('.number');
const topDiv = document.querySelector('.priceSpan');
const buttonDiv = document.querySelector('.incorrectSpan');
let span;

input.addEventListener("focus", () => {
  input.classList.add('input-focus');  
  });

input.addEventListener("blur", (e) => {
  // console.log(e.target.value);
  if (e.target.value > 0 ) {
    if (!span) {
      span = document.createElement("span");
    }
    const button = document.createElement('button');
    button.innerText = 'X';
    button.addEventListener('click', (e) => {
      e.preventDefault;
      topDiv.innerHTML = '';
      input.value = '';
    });
  
    span.innerText = `Текущая цена: ${e.target.value}`;
    topDiv.append(span);
    topDiv.append(button);
    input.classList.remove('input-focus');
    input.classList.add('green-color');
    
    
  } else {
    input.classList.remove('green-color');
    span.innerText = "Please enter correct price";
    buttonDiv.append(span);
    input.classList.add('out-focus');
    

    input.addEventListener('click', (e) => {
      e.preventDefault;
      buttonDiv.innerHTML = '';
      input.classList.remove('out-focus');
    });
  }
  
});

