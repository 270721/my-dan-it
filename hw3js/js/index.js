'use strict'; 
 
let firstNum  = +prompt("Введите первое число"); 
let secondNum  = +prompt("Введите второе число"); 
let operator = prompt("Введите знак операции"); // любой: +, -, *, / 
 
let result; 
 
const calcResult = function(firstNum, secondNum){ 
  switch(operator) { 
    case '+': 
        result = firstNum + secondNum; 
        console.dir(result); 
        break; 
    case '-': 
        result = firstNum - secondNum; 
        console.dir(result); 
        break; 
    case '*': 
        result = firstNum * secondNum; 
        console.dir(result); 
        break;   
    case '/': 
        result = firstNum / secondNum; 
        console.dir(result); 
        break; 
    default: 
        console.dir('Недоступное значение'); 
  } 
} 
calcResult(firstNum, secondNum);